import React,{Component} from 'react';

class TodoItem extends Component{
    constructor(props){
        super(props);

        this.onClickDelete = this.onClickDelete.bind(this);
    }
    
    onClickDelete(){
        this.props.onTaskDelete(this.props.task);
    }

    render(){
        return(
            <div className="rowbody">
                <div className="col-sm-2"/>
                <div className="col-sm-7 list-group-itemedit">
                    <div className="row">
                        <div className="col-sm-8 showtodo">
                            {this.props.task.todo}
                        </div>
                        <div className="col-sm-3" style="margin: 10px">
                            <button type="button" className="button-del" onClick={this.onClickDelete}>done</button>
                        </div>
                    </div>
                    <div className="rowbody">
                        <div className="col-sm-9 showdescrip" >
                            - {this.props.task.descrip}
                        </div>
                        <div className="col-sm-3 textRight">
                                {this.props.task.time}
                        </div>
                    </div>
                </div>
                <div className="col-sm-3"/>
            </div>
        );
    }
}

export default TodoItem;