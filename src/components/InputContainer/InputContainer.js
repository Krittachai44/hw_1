import React,{Component} from 'react';

class InputContainer extends Component{

    constructor(props){
        super(props);

        this.onClickAddTask = this.onClickAddTask.bind(this);
    }

    onClickAddTask(){
        if(this.refs.addTaskInput.value == ""){
            console.log("Please add some Task");
            return ;
        }
        let d = new Date();
        let date = d.toDateString();
        let detail = this.refs.addTaskDetail.value;
        let task = this.refs.addTaskInput.value;
        let h = d.getHours();
        let m = d.getMinutes();

        console.log("Add    : " + task + " " + detail + " " + date + " " + h + ":" + m);

        this.props.onTaskAdd(task,detail,date+" " + h + ":" + m );
    }



    render(){
        return(
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-2"/>
                        <div className="col-sm-8">
                            <input className="col-sm-12 Subjectbox" type="text" ref="addTaskInput" placeholder="Add new task..."/>
                        </div>
                        <div className="col-sm-2"/>
                    </div>
                    <div className="row">
                        <div className="col-sm-2"/>
                        <div className="col-sm-8">
                            <textarea className="col-sm-12 Descripbox" ref="addTaskDetail" placeholder="Add detail about your task..."/>
                        </div>
                        <div className="col-sm-2"/>
                    </div>
                    <div className="row">
                        <div className="col-sm-2"/>
                        <div className="col-sm-8">
                            <div className="col-sm-4"/>
                            <button type="button" className="col-sm-4 button-add textCenter" onClick={this.onClickAddTask}>Add</button>
                            <div className="col-sm-4"/>
                        </div>
                        <div className="col-sm-2"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default InputContainer;