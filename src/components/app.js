import React, {Component} from 'react';
import Header from './Header/Header';
import InputContainer from './InputContainer/InputContainer';
import TodoList from './TodoList/TodoList';

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            task: {
                todo : '',
                descrip : '',
                time : ''
            }
        };

        this.onTaskAdd = this.onTaskAdd.bind(this);
    }



    onTaskAdd(value,detail,date){
        this.setState({task:{todo: value,descrip: detail,time: date}});
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-2"/>
                    <div className="col-sm-8">
                        <Header/>
                        <InputContainer onTaskAdd={(value,detail,date)=>{this.onTaskAdd(value,detail,date)}}/>
                        <div className="textbody">
                            <TodoList task={this.state.task}/>
                        </div>
                    </div>
                <div className="col-sm-2"/>
            </div>
        );
    }
}

export default App;